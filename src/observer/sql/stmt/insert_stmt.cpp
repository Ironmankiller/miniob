/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/5/22.
//

#include "sql/stmt/insert_stmt.h"
#include "common/log/log.h"
#include "storage/common/db.h"
#include "storage/common/table.h"
#include "util/util.h"

InsertStmt::InsertStmt(Table *table, std::vector<std::vector<const Value *>> values, int value_amount, int entry_amount)
  : table_ (table), values_(values), value_amount_(value_amount), entry_amount_(entry_amount)
{}

RC InsertStmt::create(Db *db, const Inserts &inserts, Stmt *&stmt)
{
  const char *table_name = inserts.relation_name;
  if (nullptr == db || nullptr == table_name || inserts.values_array_num <= 0) {
    LOG_WARN("invalid argument. db=%p, table_name=%p, value_array_num=%d", 
             db, table_name, inserts.values_array_num);
    return RC::INVALID_ARGUMENT;
  }

  // check whether the table exists
  Table *table = db->find_table(table_name);
  if (nullptr == table) {
    LOG_WARN("no such table. db=%s, table_name=%s", db->name(), table_name);
    return RC::SCHEMA_TABLE_NOT_EXIST;
  }

  const TableMeta &table_meta = table->table_meta();
  const int field_num = table_meta.field_num() - table_meta.sys_field_num();
  const int sys_field_num = table_meta.sys_field_num();
  std::vector<std::vector<const Value *>> v;

  for (size_t j = 0; j < inserts.values_array_num; j++) {
    // check the fields number
    const Value *values = inserts.values[j];
    const int value_length = inserts.value_num[j];

    if (field_num != value_length) {
      LOG_WARN("schema mismatch. value len=%d, field num in schema=%d", value_length, field_num);
      return RC::SCHEMA_FIELD_MISSING;
    }

    // check fields type
    std::vector<const Value *> tmp;
    for (int i = 0; i < value_length; i++) {
      const FieldMeta *field_meta = table_meta.field(i + sys_field_num);
      const AttrType field_type = field_meta->type();
      const AttrType value_type = values[i].type;
      if (field_type != value_type) { // TODO try to convert the value type to field type
        RC rc = convert_value_type(field_type, value_type, const_cast<Value &>(values[i]));
        if (rc != SUCCESS) {
          LOG_WARN("field type misFmatch. table=%s, field=%s, field type=%d, value_type=%d", 
            table_name, field_meta->name(), field_type, value_type);
          return RC::SCHEMA_FIELD_TYPE_MISMATCH;
        }
      }
      tmp.push_back(values + i);
    }
    v.push_back(tmp);
  }
  

  // everything alright
  stmt = new InsertStmt(table, v, v[0].size(), v.size());
  return RC::SUCCESS;
}
