/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by wangyunlai on 2022/9/28
//

#pragma once

#include <string>
#include <sstream>
#include <stdlib.h>
#include <stdint.h>

#include "util/date.h"
#include "common/log/log.h"
#include "sql/parser/parse_defs.h"

std::string double2string(double v);

inline RC convert_value_type_for_compare(AttrType field_type, AttrType value_type, Value& value) 
{
  RC rc = RC::SUCCESS;

  if (field_type == DATES) {
    int32_t result = -1;
    const char* data = (const char *)value.data;
    rc = string_to_date(data, result);
    if (rc != RC::SUCCESS) {
      LOG_ERROR("Can't convert '%s' to date value", data);
      return rc;
    }
    value_destroy(&value);
    value_init_date(&value, result);
  } else if (field_type == INTS && value_type == CHARS) {
    const char *data = (const char *)value.data;
    std::stringstream ss;
    ss << data;
    float f;
    ss >> f;
    ss.clear();
    int result = static_cast<int>(f > 0 ? f+0.5 : f-0.5);
    value_destroy(&value);
    value_init_integer(&value, result);
  } else if (field_type == FLOATS && value_type == CHARS) {
    const char *data = (const char *)value.data;
    std::stringstream ss;
    ss << data;
    float f;
    ss >> f;
    ss.clear();
    value_destroy(&value);
    value_init_float(&value, f);
  }
  return rc;
}

inline RC convert_value_type(AttrType field_type, AttrType value_type, Value& value) 
{
  RC rc = RC::SUCCESS;

  if (field_type == DATES) {
    int32_t result = -1;
    const char* data = (const char *)value.data;
    rc = string_to_date(data, result);
    if (rc != RC::SUCCESS) {
      LOG_ERROR("Can't convert '%s' to date value", data);
      return rc;
    }
    value_destroy(&value);
    value_init_date(&value, result);
  } else if (field_type == INTS && value_type == FLOATS) {
    float data = *(float *)value.data;
    int result = static_cast<int>(data > 0 ? data+0.5 : data-0.5);
    value_destroy(&value);
    value_init_integer(&value, result);
  } else if (field_type == FLOATS && value_type == INTS) {
    int data = *(int *)value.data;
    float result = static_cast<float>(data);
    value_destroy(&value);
    value_init_float(&value, result);
  } else if (field_type == INTS && value_type == CHARS) {
    const char *data = (const char *)value.data;
    std::stringstream ss;
    ss << data;
    float f;
    ss >> f;
    ss.clear();
    int result = static_cast<int>(f > 0 ? f+0.5 : f-0.5);
    value_destroy(&value);
    value_init_integer(&value, result);
  } else if (field_type == CHARS && value_type == INTS) {
    std::ostringstream oss;
    int data = *(int *)value.data;
    oss << data;
    value_destroy(&value);
    value_init_string(&value, oss.str().c_str());
  } else if (field_type == FLOATS && value_type == CHARS) {
    const char *data = (const char *)value.data;
    std::stringstream ss;
    ss << data;
    float f;
    ss >> f;
    ss.clear();
    value_destroy(&value);
    value_init_float(&value, f);
  } else if (field_type == CHARS && value_type == FLOATS) {
    std::ostringstream oss;
    float data = *(float *)value.data;
    oss << data;
    value_destroy(&value);
    value_init_string(&value, oss.str().c_str());
  }
  return rc;
}
